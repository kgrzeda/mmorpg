package pl.edu.pjwstk.mpr.main;

import java.sql.*;

public class DBData {
	
	public Connection Cnn;
	public Statement mStatement;
	
	public void ConnectToDB(String xDBName){
		
		try {
		      Class.forName("org.hsqldb.jdbc.JDBCDriver");
		  } catch (Exception e) {
		      System.err.println("ERROR: failed to load HSQLDB JDBC driver.");
		      e.printStackTrace();
		      return;
		  }

		try {
			
			Cnn = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/"+xDBName, "SA", "");
			mStatement = Cnn.createStatement();
			System.out.println("Połączenie z bazą danych powiodło się!");
			
		} catch (SQLException e) {
			System.err.println("ERROR: Nie udalo sie polaczyc do bazy "+xDBName);
			e.printStackTrace();
		}
	}

	public void CreateTableGry() {
		
		String pSQL;
		int pUpdateCount;
		DatabaseMetaData pMetaData;
		ResultSet pResultSet;
		
		try{
			pMetaData = Cnn.getMetaData();
			pResultSet = pMetaData.getTables(null, null, "GRY", null);
			if (pResultSet.next()) return;						//jeśli tabela instnieje wyjście	
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		
		
		pSQL= "CREATE TABLE Gry (" +
		"Index INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY ," +
		"nazwa varchar(50)" + "postac varchar(50)" + "gildia varchar(50)" + "level varchar(50)" +
		")";

		try {
			pUpdateCount = mStatement.executeUpdate(pSQL);
			if( pUpdateCount > 0 )
				System.out.println("Udalo sie utworzyc tabele gier");
			
		} catch (SQLException e) {
			System.err.println("ERROR: Nie udalo sie utworzyc tabeli gier");
			e.printStackTrace();
		}
		
	}
	
	public ResultSet GetResultSet(String xSQL){
		//funkcja zwracająca rekordy za pomocą obiektu ResultSet
		
		ResultSet pRecordSet;
		
		pRecordSet= null;
		
		try{
			pRecordSet = mStatement.executeQuery(xSQL);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		
		
		return pRecordSet;
		
	}

	public boolean ExecuteUpdateQuery(String xSQL){
		//funkcja wykonująca zapytanie dodające rekord
		
		try {
			mStatement.execute(xSQL);
		} catch (SQLException e) {
			System.err.println("Wykonanie zapytania nie powiodło się: " +xSQL);
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}	

}
