package pl.edu.pjwstk.mpr.main;

import java.sql.*;

public class Gra {
	
	public int Index;
	public String nazwa;
	public String postac;
	public String gildia;
	public String level;
	public DBData Baza;
	
	public Gra (DBData xBaza, int xIndex, String xnazwa, String xpostac, String xgildia, String xlevel) {
		Index = xIndex;
		nazwa = xnazwa;
		postac = xpostac;
		gildia = xgildia;
		level = xlevel;
		Baza = xBaza;
	}

	public void SaveGraToDB() {
		//funkcja zapisująca gre do bazy
	
		ResultSet pResultSet;
		String pSQL;
		PreparedStatement pIdentity;
		PreparedStatement pPrparedStatement;
		ResultSet pResult;
		
		pResultSet = Baza.GetResultSet("SELECT * FROM Gry WHERE nazwa = '"+this.nazwa+"'");
		
		try {
			if(pResultSet.next()) return;					//jeśli gra już istnieje
		} catch (SQLException e) {
		}			
		
		try {
		  pPrparedStatement = Baza.Cnn.prepareStatement("INSERT INTO Gry (nazwa) VALUES (?)");
		  pPrparedStatement.setString(1, this.nazwa);
		  pPrparedStatement.setString(2, this.postac);
		  pPrparedStatement.setString(3, this.gildia);
		  pPrparedStatement.setString(4, this.level);
		  pPrparedStatement.executeUpdate();
	      pIdentity = Baza.Cnn.prepareStatement("CALL IDENTITY()");
	      pResult = pIdentity.executeQuery();
	      pResult.next();
	      this.Index = pResult.getInt(1);
	      pResult.close();
		} catch (SQLException e) {
			
		}

	
//	public String toString() {
//		return "Gra: "+nazwa+" postac: "+postac+" gildia:"+gildia+" poziom:"+level;
//	}
	}

}
